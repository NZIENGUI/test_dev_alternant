<?php

namespace App\Controller;

use AppendIterator;
use App\Entity\Order;
use App\Entity\Customer;
use App\Entity\OrderLine;
use App\Repository\CustomerRepository;
use App\Service\OrderConsumer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Serializer;


class LengowOrderController extends AbstractController
{
     
    
    /**
     * @Route("/orders/last", name="lengow_orders_last")
     */
    public function ordersLast()
    {
        //
        // Question 1 :
        //
        // - Lister les 20 dernières commandes dont le statut est "new"
        //
        // -> Implémenter la méthode getLastNewOrders()
        // -> Utiliser le queryBuilder, trier par date et id decroissant.
        //

        $orders = $this->getDoctrine()->getRepository(Order::class)
                ->getLastNewOrders()
        ;
        return $this->render('lengow_order/new_orders.html.twig', [
            'orders' => $orders,
        ]);
    }


    /**
     * @Route("/orders/last_optimized", name="lengow_orders_last_optimized")
     */
    public function ordersLastOptimized()
    {
        //
        // Question 2 :
        //
        // - Lister les 20 dernières commandes dont le statut est "new", version optimisée
        //
        // -> Implémenter la méthode getLastNewOrdersOptimized()
        // -> Utiliser le queryBuilder, trier par date et id decroissant.
        //
        // -> La page ne doit générer qu'une seule requête SQL
        //

        $orders = $this->getDoctrine()->getRepository(Order::class)
                       ->getLastNewOrdersOptimized()
        ;

        return $this->render('lengow_order/new_orders.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/orders/new", name="lengow_orders_new")
     */
    public function ordersNew(SerializerInterface $serializer,Request $request,CustomerRepository $customer)
    {
        //
        // Question 3 :
        //
        // - Consommer l'API /api/orders/new et enregistrer les nouvelles commandes en base
        //
        // -> Effectuer le travail le plus simplement dans le controlleur.
        // -> Afficher le nombre total de commandes dont le statut est "new"
        //
        
       //Consomme L'API
       $listOrderNew=file_get_contents("https://flanord-nziengui.com/lengow/public/api/orders/new",true);
       $someArray=$serializer->decode($listOrderNew,'json');
    
        // Enregistrement en BDD
        foreach ($someArray as $key => $value) {
            
            foreach ($value['date'] as $keys => $valuedata) {

          }
                 $customers=$customer->findOneBy(array('id'=>$value["customer_id"]));
                 $entityManager = $this->getDoctrine()->getManager();
                 $order=new Order();
                 $order->setCustomer($customers);
                 //$order->setCreatedAt(\DateTime::createFromFormat('Y-m-d',$valuedata));
                 $order->setStatus('new');

               
                 foreach ($value['orderlines'] as $keys => $valueorderlines) {

                    
                }
               /* $entityManager->persist($order);
                $entityManager->flush();*/

                
    }


        // Décompte des commandes dont le statut est "new"
        $totalNewOrders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->countNewOrders()
        ;

        return $this->render('lengow_order/get_new_orders.html.twig', [
            'total_new_orders' => $totalNewOrders,
        ]);
    }

    /**
     * @Route("/orders/new_service", name="lengow_orders_new_service")
     */
    public function ordersNewService(OrderConsumer $orderConsumer,SerializerInterface $serializer)
    {
        //
        // Question 4 :
        //
        // - Consommer l'API /api/orders/new et /api/orders/new_xml et enregistrer les nouvelles commandes en base
        //   en utilisant les services.
        //
        // -> Le service prendra en paramètre l'url de l'API et devra traiter les commandes quelque soit le format de l'API (JSON et XML).
        // -> L'ajout d'un ou plusieurs autres format d'API ne devra générer aucune modification de la classe de service.
        //


        // Consommation de l'API Json
         $OrdersNew=file_get_contents("https://flanord-nziengui.com/lengow/public/api/orders/new",true);
         $OrderListe=$serializer->decode( $OrdersNew,'json');


        // Consommation de l'API XML
        $OrdersNewXml=simplexml_load_file("https://flanord-nziengui.com/lengow/public/api/orders/new_xml");
       
        //dump($OrdersNewXml);

        //die();

        // Décompte des commandes dont le statut est "new"
        $totalNewOrders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->countNewOrders()
        ;

        return $this->render('lengow_order/get_new_orders.html.twig', [
            'total_new_orders' => $totalNewOrders,
        ]);
    }

    /**
     * @Route("/orders/jquery", name="lengow_orders_jquery")
     */
    public function orderJqueryLoad(SerializerInterface $serializer)
    {
        //
        // Question 5 :
        //
        // - Consommer l'API /api/orders/random en "Ajax" pour récupérer des commandes.
        //
        // -> Utiliser jQuery pour effectuer la requête Ajax
        // -> Filtrage en javascript des résultats sur la sélection du statut.
        //

         $listOrderNew=file_get_contents("https://flanord-nziengui.com/lengow/public/api/orders/random",true);
         $OrderNewData=$serializer->decode($listOrderNew,'json');




        return $this->render('lengow_order/jquery_orders.html.twig');
    }

    /**
     * @Route("/orders/vanilla_js", name="lengow_orders_vanilla_js")
     */
    public function orderVanillaJsLoad(SerializerInterface $serializer)
    {
        //
        // Question 6 :
        //
        // - Consommer l'API /api/orders/random en "Ajax" pour récupérer des commandes.
        //
        // -> Utiliser du pure javascript pour effectuer tous les traitements (chargement, affichage, filtre).
        // -> Doit être fonctionnel sous Chrome
        //
        
        /**
         * consommation de l'api
         */
        $listOrderNew=file_get_contents("https://flanord-nziengui.com/lengow/public/api/orders/random",true);
        $OrderNewData=$serializer->decode($listOrderNew,'json');

         
        


        return $this->render('lengow_order/vanilla_js_orders.html.twig');
    }
}
