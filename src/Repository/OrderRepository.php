<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * Retourner les 20 dernières commandes
     * @return array
     *
     * -> utiliser le query builder
     *
     */
    public function getLastNewOrders($value='New'): array
    {
        return $this->createQueryBuilder('F')
            ->andWhere('F.status = :val')
            ->setParameter('val', $value)
            ->orderBy('F.id','DESC', 'F.created_at ','DESC')
            ->setMaxResults(20)
            ->getQuery()
            ->getResult();
    }


    /**
     * Retourner les 20 dernières commandes, version optimisée (1 seule reqête générée)
     * @return array
     *
     * -> utiliser le query builder
     *
     */
    public function getLastNewOrdersOptimized(): array
    {
        return $this->createQueryBuilder('F')
            ->andWhere('F.status = :val')
            ->setParameter('val','New')
            ->orderBy('F.id','DESC')
            ->setMaxResults(20)
            ->getQuery()
            ->getResult();

    }

    /**
     * Décompte les commandes dans le status 'new'
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * -> utiliser le query builder
     * utilisation de getSingleScalarResult me permet de  retourner qu’une seule valeur.
     */
    public function countNewOrders(): int
    {
        return $this->createQueryBuilder('F')
        ->select('COUNT(F.id)')
        ->andWhere('F.status = :val')
        ->setParameter('val','new')
        ->getQuery()
        ->getSingleScalarResult();   

    }

    /**
     * Retourne 50 commandes (aléatoire)
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRandomOrders(): array
    {
        $sql = 'SELECT o.*, c.* 
            FROM lgw_test_order AS o
              JOIN lgw_test_orderline AS ol ON ol.order_id = o.id 
              JOIN lgw_test_customer AS c ON o.customer_id = c.id
            ORDER BY RAND()
            LIMIT 50
         ';

        $stmt = $this->getEntityManager()
            ->getConnection()
            ->executeQuery($sql)
        ;

        return $stmt->fetchAll();
    }
}
